# BillingApp

## Getting started

This is a simple project that integrates with the Google Billing library in order to exemplify and 
reproduce an issue recently found while testing the purchase flow on one of our gaming apps. 

- You can find the source code in this repository. It's a public repository, you should be able to access it.
- The app presents the latest billing status code and a button to make an InApp purchase.
- The app only deals with InApp purchases, therefore subscriptions are not supported here.
- All source code can be found in the `java/com.example.billingapp/MainActivity` file.
- The layout is all set in the `res/layout/activity_main.xml` file.

**Attention**: Before running the app, please replace the `applicationId` value (`com.cronus.packagesdemo`) in the `build.gradle (Module: app)` file 
with a bundle identifier you have access to and that has InApp products configured. You must also replace the product identifier `com.cronus.packagesdemo.iap.tier2` 
with the product identifier of an InApp product you have set up for your bundle identifier previously set in the `build.gradle` file.

If you have any trouble cloning this repository, please reach out to us. 

## The Billing Service Disconnection Issue 
According to Google documentation, the developers are [recommended to implement their own retry logic](https://developer.android.com/google/play/billing/integrate#connect_to_google_play) 
inside onBillingServiceDisconnected method. However, Google documentation also states that [Google already provides a retry backoff mechanism](https://developer.android.com/reference/com/android/billingclient/api/BillingClientStateListener#onBillingServiceDisconnected()) 
that will handle retries automatically, continuously retrying to reconnect to the Billing service until it succeeds.

After testing both statements, we noticed that Google indeed handles retries to reconnect to the Billing service when the service gets disconnected. 
However, we found a corner case where Google seems to get struck retrying and failing, making it impossible for the user to make purchases and this 
negatively impacts the user experience and might impact revenue. 

Here is the workaround we found for this issue: the user must close the app, open the Play Store and then reopen the app,
repeating this process a few times (it varies, it might take 5 or more attempts) until Google is able to successfully reconnect to the Billing Service, 
re-enabling purchases for the app.

## How to reproduce the Billing Service Disconnection Issue?

1. Install the app on your device.
2. Open the app on your device.
3. **Change the language/region** of the device through `Settings` and **switch back to the app**.
4. Try making a purchase by clicking on the "make purchase" button.
5. Repeat the **previous two steps** at least **3 times** until the Google Billing service gets **disconnected**. 
6. Notice that Google will now be retrying over and over again to reestablish the connection to the Billing service, but 
the disconnection keeps happening after each retry attempt from Google. 
**How to observe this behavior?** Please check the logs for the app on Android Studios. 

Now, please follow the following steps to fix the issue (workaround):
1. Close the app.
2. Open the Play Store.
3. Reopen the app and try making a purchase.
4. Repeat the **three steps above as many times as needed** until Google succeeds to reestablish and maintain a connection to the Billing Service.
5. Successfully make a purchase.