package com.example.billingapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.android.billingclient.api.*
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.collect.ImmutableList
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.purchase_button)
            .setOnClickListener {
                Log.d("Billing", "User started a purchase")
                purchaseProduct()
            }
        setupBillingService()
    }

    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
                    Log.d(
                        "Billing",
                        "Finished Purchase: $purchase"
                    )
                }
            } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                // Handle an error caused by a user cancelling the purchase flow.
                Log.e(
                    "Billing",
                    "User canceled the purchase."
                )
            } else {
                // Handle any other error codes.
                Log.e(
                    "Billing",
                    "Failed finishing purchase: error code = ${billingResult.responseCode}"
                )
            }

            billingStatusCode = billingResult.responseCode
            var billingStatusText = findViewById<TextView>(R.id.billingStatus)
            billingStatusText.text = "Billing Status (Response Code): $billingStatusCode"
        }

    private val billingClient by lazy {
        BillingClient.newBuilder(this@MainActivity)
            .setListener(purchasesUpdatedListener)
            .enablePendingPurchases()
            .build()
    }

    private var billingStatusCode = BillingClient.BillingResponseCode.BILLING_UNAVAILABLE
    private var loadedInAppProducts: List<ProductDetails> = emptyList()

    private fun purchaseProduct() {
        // Implement here the logic to trigger a purchase for the user.
        if (billingStatusCode == BillingClient.BillingResponseCode.OK && loadedInAppProducts.isNotEmpty()) {
            // Implement the purchase flow here.
            val productDetailsParamsList = listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    // retrieve a value for "productDetails" by calling queryProductDetailsAsync()
                    .setProductDetails(loadedInAppProducts[0])
                    .build()
            )

            val billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(productDetailsParamsList)
                .build()

            // Launch the billing flow.
            val billingResult = billingClient.launchBillingFlow(this@MainActivity, billingFlowParams)
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                Log.d(
                    "Billing",
                    "Succeeded purchasing product (result code ${billingResult.responseCode}): ${loadedInAppProducts[0]}"
                )
            } else {
                Log.d(
                    "Billing",
                    "Failed purchasing product (error code ${billingResult.responseCode}): ${loadedInAppProducts[0]}"
                )
            }
        } else {
            Log.e(
                "Billing",
                "Failed purchasing product (error code $billingStatusCode): # loaded products = ${loadedInAppProducts.size}"
            )
        }
    }

    private fun setupBillingService() {
        // Implement here the logic for setting up the billing service.
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    val queryProductDetailsParams =
                        QueryProductDetailsParams.newBuilder()
                            .setProductList(
                                ImmutableList.of(
                                    QueryProductDetailsParams.Product.newBuilder()
                                        .setProductId("com.cronus.packagesdemo.iap.tier2")
                                        .setProductType(BillingClient.ProductType.INAPP)
                                        .build()))
                            .build()

                    billingClient.queryProductDetailsAsync(queryProductDetailsParams) {
                            billingResult,
                            productDetailsList ->
                        val inAppResponseCode = billingResult.responseCode
                        Log.d(
                            "Billing",
                            "Querying SKU Details Response (InApp) => response code: $inAppResponseCode")

                        if (inAppResponseCode == BillingClient.BillingResponseCode.OK) {
                            loadedInAppProducts = productDetailsList
                            Log.d(
                                "Billing",
                                "Adding products, totalling ${loadedInAppProducts.size} products"
                            )
                        } else {
                            Log.e(
                                "Billing",
                                "Failed querying InApp product details $inAppResponseCode"
                            )
                        }
                    }
                }

                billingStatusCode = billingResult.responseCode
                var billingStatusText = findViewById<TextView>(R.id.billingStatus)
                billingStatusText.text = "Billing Status (Response Code): $billingStatusCode"
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                billingStatusCode = BillingClient.BillingResponseCode.SERVICE_DISCONNECTED
                Log.e(
                    "Billing",
                    "Billing Service disconnected."
                )

                var billingStatusText = findViewById<TextView>(R.id.billingStatus)
                billingStatusText.text = "Billing Status (Response Code): $billingStatusCode"
            }
        })
    }
}